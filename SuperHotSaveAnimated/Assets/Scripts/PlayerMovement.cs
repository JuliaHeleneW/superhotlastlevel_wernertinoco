﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    bool shootingFinished=true;
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("space")&&shootingFinished==true) {
			shootingFinished = false;
			anim.SetBool ("shootEnable", true);
			Debug.Log ("In");
			Invoke ("shootingEnable", 1f);
			Debug.Log ("Out2");
		}else {
			anim.SetBool ("shootEnable", false);
			//anim.SetBool ("shootDisable", true);
		}
	}

    void shootingEnable()
    {
        shootingFinished = true;
		Debug.Log ("Out1");
		anim.SetBool ("shootEnable", false);
    }
}
