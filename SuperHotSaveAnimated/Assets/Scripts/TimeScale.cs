﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public 
class TimeScale : MonoBehaviour {
	public float tDelta;
	public float tFixedDelta;
	public float gameTimeScale = 1;
	// Use this for initialization

	public static TimeScale instance;

	public static TimeScale GetInstance(){
		return instance;
	}

	void Awake(){
		instance = this;
	}
	void Start () {

	}
	// Update is called once per frame
	void Update () {
		tDelta = Time.deltaTime * gameTimeScale;
	}

	void FixedUpdate(){
		tFixedDelta = Time.fixedDeltaTime * gameTimeScale;
	}
}
