﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
	//public GameObject[] npcs=new GameObject[3];
	public List<GameObject> npcs;
	public GameObject npcInput;
	public Transform floor;
	const int MAX_NPCs=3;
	const int MAX_ATTEMPTS = 100;
	const string NPC_TAG = "NPC";
	// Use this for initialization
	void Start () {
		//Transform[] tiles = floor.GetComponentsInChildren<Transform> (); delete used object to increase efficiency
		for (int i = 0; npcs.Count < MAX_NPCs&&i<MAX_ATTEMPTS; i++) {
			Vector3 placement = floor.GetChild (UnityEngine.Random.Range (0, floor.childCount)).position;
			bool good_placement = true;
			foreach (GameObject npc in npcs) {
				Vector3 origin = placement+new Vector3(0,1.3f,0);
				Vector3 target = npc.transform.position+new Vector3(0,1.3f,0);
				Vector3 direction = target - origin;
				RaycastHit hit;
				if (Physics.Raycast (origin, direction, out hit, 200)) {
					if (hit.transform.tag == NPC_TAG||hit.transform.tag=="Player") {
						good_placement = false;
						Debug.Log ("Oops, we are in tha same room!");
					}
				}
			}
			if (good_placement == true) {
				npcs.Add (Instantiate (npcInput, placement, Quaternion.identity));
			}
		}
		/*for (int i = 0; i < npcs.Count; i++) {
			//instantiate NPC with position of FindPosition
			npcs[i]=Instantiate(npcInput,findPosition(),Quaternion.identity);
		}*/
	}
	
	// Update is called once per frame
	/*void Update () {
		
	}

	public Vector3 findPosition()
	{
		//find a random position:spawn on random floor tile
		Transform[] children=floor.GetComponentsInChildren<Transform>();
		Transform randomObject;
		do {
			randomObject=children[Random.Range(0,children.Length)];
		} while(checkPos(randomObject)==false);
		//make sure characters don't touch each other
		//find, if other NPC is within same room
		return randomObject.transform.position;
	}

	public Quaternion findAngle()
	{
		float angle = Random.Range (0, 360);
		Debug.Log (angle);
		return new Quaternion (0, angle, 0, 0);
	}
	public bool checkPos(Transform npcPos)
	{
		//Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, weaponRange)
		RaycastHit hit;
		if (Physics.Linecast(npcPos.transform.position,Vector3.forward,out hit)) {
			if (hit.collider.tag == "Wall") {
				return true;
			} else {
				return false;
			}
		} 
		//LineCast?
		else {
			return true;
		}
	}*/
}
