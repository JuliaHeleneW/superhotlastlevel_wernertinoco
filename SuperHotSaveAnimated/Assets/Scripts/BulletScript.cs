﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class BulletScript : MonoBehaviour {

    //public Transform gunEnd;
    //public Transform gunPos;
    private TrailRenderer laserLine;
    public Animator anim;
    GameObject player;

    GameObject fpsCam;
    CharacterController cCon;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsC;
    public GameObject playerCam;
    Vector3 lookPosition;
    CapsuleCollider cCol;

    //public TriangleExplosion npcDead;
    //bool reached;
    private WaitForSeconds dyingDuration; //= new WaitForSeconds(1f);
    // Use this for initialization
    void Start () {
        //float direction = 1000f * Time.deltaTime;
        //gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0.1f, 0.1f, 0.1f);
        Debug.Log(gameObject.GetComponent<Rigidbody>().velocity);
        //gameObject.GetComponent<Rigidbody>().velocity /= 1000;
        //transform.Translate(0,0,direction);
        anim = GameObject.FindWithTag("PlayerAnim").GetComponent<Animator>();
        cCon = GameObject.FindWithTag("PlayerAnim").GetComponent<CharacterController>();
        fpsCam = GameObject.Find("FirstPersonCharacter");
        laserLine = GetComponent<TrailRenderer>();

        player = GameObject.FindGameObjectWithTag("Player");
        cCon = GameObject.FindWithTag("PlayerAnim").GetComponent<CharacterController>();
        fpsCam = GameObject.Find("FirstPersonCharacter");
        fpsC = GameObject.FindWithTag("PlayerAnim").GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        playerCam = GameObject.Find("PlayerCamera");
        cCol = GameObject.FindWithTag("PlayerAnim").GetComponent<CapsuleCollider>();
    }

    //void Update()
    //{
    //    if(destinate!=null)
    //    {
    //        float step = 100f * Time.deltaTime;
    //        transform.position = Vector3.MoveTowards(transform.position,destinate.position, step);
    //    }
    //}

    // Update is called once per frame
    //void Update () {
    //       Debug.Log(reached);
    //       Debug.Log(transform.position);
    //       Debug.Log(gunPos.position);
    //       if (Vector3.Distance(transform.position,gunPos.position) >= 2f)
    //       {
    //           reached = true;

    //       }
    //       if(reached==true)
    //       {
    //           gunPos.position += Vector3.forward;
    //           laserLine.SetPosition(0, gunPos.position);
    //       }
    //       laserLine.SetPosition(1, transform.position);
    //   }

    private void Update()
    {
        lookPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.transform.tag);
        if(collision.transform.tag=="Player"|| collision.transform.tag == "PlayerAvatar"|| collision.transform.tag == "PlayerAnim")
        {
            Debug.Log("animIn");
            if (anim.GetBool("Dying") == false)
            {
                anim.SetBool("Dying", true);
                StartCoroutine(DyingAnim());
            }
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else if (collision.transform.tag == "NPC")
        {
            Debug.Log("animInNPC");
            collision.transform.root.GetComponentInChildren<TriangleExplosion>().hit=true;
        }
        gameObject.GetComponent<TrailRenderer>().time=0f;
        gameObject.GetComponent<TrailRenderer>().enabled=false;
        Destroy(gameObject);
    }

    private IEnumerator DyingAnim()
    {
        lookPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
        fpsCam.transform.LookAt(lookPosition);
        playerCam.transform.position = fpsCam.transform.position;
        fpsCam.SetActive(false);
        playerCam.GetComponent<DyingCamera>().scriptActive = true;
        //fpsCam.GetComponent<Camera>().fieldOfView *= 10f;
        cCon.enabled = false;
        cCol.enabled = true;
        fpsC.enabled = false;
        yield return new WaitForSeconds((anim.GetCurrentAnimatorStateInfo(0).length) + 0.5f);
        Debug.Log(anim.GetCurrentAnimatorStateInfo(0).length);
        //Time.timeScale = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
