﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RaycastShooting : MonoBehaviour {
	public int gunDamage = 1;                                           
	public float fireRate = 0.25f;                                      
	public float weaponRange = 70f;                                     
	public float hitForce = 500000f;                                      
	public Transform gunEnd;
	public Animator anim;

	private Camera fpsCam;                                              
	private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);    
	//private AudioSource gunAudio;                                       
	private LineRenderer laserLine;                                     
	private float nextFire;  

	// Use this for initialization
	void Start () {
        fpsCam = transform.parent.GetComponentInChildren<Camera>();
        laserLine = GetComponent<LineRenderer>();
		anim = GetComponentInParent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        //if (anim.GetCurrentAnimatorStateInfo(0).IsName("WeaponRaise"))
        //{
        //    anim.SetBool("playerShootBool", false);
        //}
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("WeaponRaise"))
        {
            anim.SetBool("playerShootBool", false);
        }
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire&& anim.GetCurrentAnimatorStateInfo(0).IsName("WeaponRaise")) 
		{
            anim.SetBool("playerShootBool", true);
			nextFire = Time.time + fireRate;
			StartCoroutine (ShotEffect());
			float offsetAngle = Random.Range (-0.001f, 0.001f);
			Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3(0.5f, 0.5f, 0.0f));
			RaycastHit hit;
			laserLine.SetPosition (0, gunEnd.position);
			if (Physics.Raycast (rayOrigin, new Vector3(fpsCam.transform.forward.x+offsetAngle,fpsCam.transform.forward.y,fpsCam.transform.forward.z), out hit, weaponRange))
			{
				laserLine.SetPosition (1, hit.point);
				//anim.Play ("PlayerAnimation2");
				//if (hit.rigidbody!=null)
				//{
				//	Rigidbody force=hit.transform.gameObject.GetComponent<Rigidbody>();
				//	force.AddForce(new Vector3(20000f,9999f,20000f));
				//	force.AddTorque (transform.up*200000f);
				//	force.AddTorque (transform.right*200000f);
				//	force.GetComponent<NPCLookRotation>().gotHit=true;
				//	Destroy (hit.transform.gameObject,1f);
				//	//Mesh Destruction
				//}
			}
			else
			{
				laserLine.SetPosition (1, rayOrigin + (fpsCam.transform.forward * weaponRange));
			}
		}
    }

	private IEnumerator ShotEffect()
	{
		laserLine.enabled = true;
		yield return shotDuration;
		laserLine.enabled = false;
	}
}
