﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public Rigidbody bulletPrefab;  // References the bullet prefab
    public Transform gunEnd;        // The location in which the bullet will shoot out of
    public float fireRate = 1f;     // The rate of fire in which the gun can shoot
    public float weaponRange = 70f; // The range in which the bullet can travel
    
    private Camera fpsCam;
    private Rigidbody bulletInstance;
    private Animator anim;
    private bool shotDone;
    private bool shootEnable;
    private float nextFire;
    private int counter;

    // Use this for initialization
    void Start()
    {
        anim = GetComponentInParent<Animator>();
        fpsCam = transform.parent.GetComponentInChildren<Camera>();
        counter = 7;
        shootEnable = false;
    }

    // Update is called once per frame
    void Update()
    {
        float offsetAngle = Random.Range(-0.001f, 0.001f);
        Vector3 target = new Vector3(fpsCam.ViewportToWorldPoint(transform.forward).x + offsetAngle, fpsCam.ViewportToWorldPoint(transform.forward).y + offsetAngle, fpsCam.ViewportToWorldPoint(transform.forward).z + offsetAngle);
        AnimatorStateInfo currentState = anim.GetCurrentAnimatorStateInfo(0);

        if (Input.GetButtonDown("Fire1") && currentState.IsName("WeaponRaise") && counter > 0 && shootEnable == false)
        {
            anim.SetBool("playerShootBool", true);
            shootEnable = true;
        }

        if (currentState.IsName("WeaponShoot") &&  shootEnable == true)
        {
            Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
            RaycastHit hit;

            if (Physics.Raycast(rayOrigin, new Vector3(fpsCam.transform.forward.x, fpsCam.transform.forward.y, fpsCam.transform.forward.z), out hit, weaponRange))
            {
                bulletInstance = Instantiate(bulletPrefab, gunEnd.position, bulletPrefab.rotation) as Rigidbody;
                bulletInstance.transform.LookAt(hit.transform.position);
                bulletInstance.AddForce(bulletInstance.transform.forward * 1000f);
                shootEnable = false;
                anim.SetBool("playerShootBool", false);
            }
            counter--;
        }
    }

}
