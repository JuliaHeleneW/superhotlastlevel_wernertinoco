﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patroling : MonoBehaviour {
    public Transform[] waypoints;
    Transform waypoint;
    //int currentWaypointIndex = 0;
    NavMeshAgent agent;
    const int EPSILON=1;
    Animator anim;
    public GameObject player;
    public Transform playerTrans;
    public float maxRange = 2.5f;
    public float minRange = 2f;
    float speed = 1f;
    Vector3 lookPosition;
    public bool gotHit = false;
    int counter = 0;
    bool inEyesight;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        gotHit = false;
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
        anim.SetBool("walk", true);
        waypoint = waypoints[Random.Range(0, waypoints.Length)];
        agent.SetDestination(waypoint.position);
        agent.stoppingDistance = 2f;
        inEyesight = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("WHAT ARE YOU LOOKING AT?!");
        if (other.gameObject.tag == "Player")
        {
            lookPosition = new Vector3(playerTrans.position.x, 0f, playerTrans.position.z);
            transform.LookAt(lookPosition);
            agent.SetDestination(playerTrans.position);
            anim.SetTrigger("run");
            inEyesight = true;
        }
    }
    //private void OnTriggerExit(Collider other)
    //{
    //    Debug.Log("STOP LOOKING!!!");
    //    if (other.gameObject.tag == "Player")
    //    {
    //        inEyesight = false;
    //    }
    //}

    // Update is called once per frame
    void Update () {
        //lookPosition = new Vector3(playerTrans.position.x, 0f, playerTrans.position.z);
        //Vector3 origin = transform.position;
        //Vector3 target = new Vector3(playerTrans.transform.position.x, transform.position.y, playerTrans.transform.position.z);
        //if ((Vector3.Distance(origin, target) <= maxRange) && (Vector3.Distance(origin, target) > minRange) && gotHit == false)
        //{

        //    transform.LookAt(lookPosition);
        //    //agent.SetDestination(playerTrans.position);
        //}
        //counter++;
        if ((transform.position-waypoint.position).magnitude<=EPSILON&&agent.destination!=playerTrans.position)
        {
            //anim.Play("Idle_01");
            //if(!anim.)
            //anim.SetBool("walk", true);
            if (counter==30)
            {
                Transform temp;
                do
                {
                    temp = waypoints[Random.Range(0, waypoints.Length)];
                }while (temp.position == waypoint.position);
                waypoint = temp;
                agent.SetDestination(waypoint.position);
                //agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].position);
                anim.SetBool("walk", true);
                counter = 0;
            }
            else
            {
                //2, 4, 5
                counter++;
                anim.SetBool("walk", false);
            }
        }
        else if(agent.destination != playerTrans.position)
        {
            anim.SetBool("walk", true);
        }
        if(inEyesight==true)
        {
            lookPosition = new Vector3(playerTrans.position.x, 0f, playerTrans.position.z);
            transform.LookAt(lookPosition);
            agent.SetDestination(playerTrans.position);
            Vector3 target = new Vector3(playerTrans.transform.position.x, transform.position.y, playerTrans.transform.position.z);
            if ((Vector3.Distance(transform.position, target) < 2.5f) && (Vector3.Distance(transform.position, target) >= minRange))
            {
                //Debug.Log("running");
                anim.SetBool("running", false);
                anim.SetBool("attacking", true);
                anim.ResetTrigger("run");
            }
            else
            {
                //Debug.Log("attack");
                anim.SetBool("running", true);
                anim.SetBool("attacking", false);
            }
                //Vector3 target = new Vector3(playerTrans.transform.position.x, transform.position.y, playerTrans.transform.position.z);
                //if ((Vector3.Distance(transform.position, target) < maxRange) && (Vector3.Distance(transform.position, target) >= minRange))
                //{
                //    //anim.ResetTrigger("run");
                //    //anim.SetTrigger("attack");
                //    anim.SetBool("running", false);
                //    anim.SetBool("attacking", true);
                //    lookPosition = new Vector3(playerTrans.position.x, 0f, playerTrans.position.z);
                //    transform.LookAt(lookPosition);
                //    agent.stoppingDistance = 1f;
                //    agent.SetDestination(playerTrans.position);
                //}
                //else if (Vector3.Distance(transform.position, target) >= maxRange)
                //{
                //    //anim.ResetTrigger("attack");
                //    //anim.SetTrigger("run");
                //    anim.SetBool("running", true);
                //    anim.SetBool("attacking", false);
                //    lookPosition = new Vector3(playerTrans.position.x, 0f, playerTrans.position.z);
                //    transform.LookAt(lookPosition);
                //    agent.stoppingDistance = 1f;
                //    agent.SetDestination(playerTrans.position);
                //}
            }
    }
}
