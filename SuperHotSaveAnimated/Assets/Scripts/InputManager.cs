﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
	TimeScale tsInstance;
	float targetTimescale = 0;
	float lerpPercentage=2f;
	// Use this for initialization
	void Start () {
		tsInstance = TimeScale.GetInstance ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D)) {
			targetTimescale = 1f;
			lerpPercentage = 10f;
		} else {
			targetTimescale = 0.0f;
			lerpPercentage = 4f;
		}
		tsInstance.gameTimeScale = Mathf.Lerp (tsInstance.gameTimeScale, targetTimescale, Time.deltaTime * lerpPercentage);
	}
}
