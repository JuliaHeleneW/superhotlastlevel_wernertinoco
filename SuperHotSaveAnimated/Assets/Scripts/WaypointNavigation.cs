﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointNavigation : MonoBehaviour {
	public Transform[] waypoints;
	//int currentWaypointIndex = 0;
	const int EPSILON=1;
	static int index;
	Vector3 lookPosition;
	float speed=5f;
	// Use this for initialization
	void Start () {
		index = 1;
		transform.position = new Vector3(waypoints [0].position.x,transform.position.y,waypoints[0].position.z);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 origin = transform.position;
		Vector3 target = new Vector3(waypoints[index].transform.position.x, transform.position.y, waypoints[index].transform.position.z);
		if (Vector3.Distance(origin, target) <= EPSILON&&index<waypoints.Length-1)
		{
			index++;
		}	
		lookPosition = new Vector3 (waypoints[index].position.x, transform.position.y, waypoints[index].position.z);
		float step = speed * Time.deltaTime;
		if (Vector3.Distance(origin, target) > EPSILON&&index<waypoints.Length-1)
		{
			transform.position =Vector3.MoveTowards(origin,target,step);
			transform.LookAt (lookPosition);
		}		
	}
}
