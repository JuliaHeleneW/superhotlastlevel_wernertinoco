﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DyingCamera : MonoBehaviour {

    Camera oldCam;
    public Camera playerCam;
    public GameObject player;
    public bool scriptActive;
    bool one;
   // public Animation camAnim;
	// Use this for initialization
	void Start () {
        oldCam = GameObject.Find("FirstPersonCharacter").GetComponent<Camera>();
        playerCam.transform.position= oldCam.transform.position;
        player = GameObject.Find("MHumanPlayer");
        scriptActive = false;
        one = true;
        //playerCam.transform.LookAt(player.transform);
        //oldCam.enabled=false;
	}
	
	// Update is called once per frame
	void Update () {
        if (scriptActive == true&&one==true)
        {
            playerCam.transform.LookAt(player.transform);
            //playerCam.transform.position = new Vector3(playerCam.transform.position.x,Mathf.Lerp(playerCam.transform.position.y, 12.04f, 0.1f), playerCam.transform.position.z);
            //camAnim.Play();
            //scriptActive = false;
            one = false;
        }
        if(scriptActive == true && one ==false)
        {
            float speed;//if(transform.position.y<12.04f)
            //{
            //    speed = 0.1f;
            //}
            speed = ((playerCam.transform.position.y < 6.04f) ? 0.1f : 0f);
            playerCam.transform.position = new Vector3(playerCam.transform.position.x, playerCam.transform.position.y+0.1f, playerCam.transform.position.z);
        }
    }
}
