﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCLookRotation : MonoBehaviour {
    //purpose: to rotate the NPC, so it looks in the direction of the player
    public GameObject player;
    public Transform playerTrans;
    public float maxRange = 30f;
    public float minRange = 2f;
    float speed = 1f;
    Vector3 lookPosition;
    public bool gotLookedAt = false;
    NavMeshAgent agent;
    float bulletDuration;

    // Use this for initialization
    void Start()
    {
        gotLookedAt = false;
        agent = GetComponent<NavMeshAgent>();
        //player = GameObject.FindGameObjectWithTag("Player");
        //playerTrans = player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("MHumanPlayer");
        playerTrans = player.transform;
        lookPosition = new Vector3(playerTrans.position.x, transform.position.y, playerTrans.position.z);
        transform.LookAt(lookPosition);
    }
}
