﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCCreate : MonoBehaviour
{
    //array of game objects for the npc clones
    public List<GameObject> npcs;
    public GameObject npcInput;
    public GameObject npcInputShoot;
    public GameObject Player;
    public List<Transform> spawnpointsCloseRange;
    public List<Transform> spawnpointsFarRange;
    public List<Transform> spawnpointsPlayer;
    public List<int> intList;

    const int MAX_NPCs = 6;
    const int MAX_ATTEMPTS = 100;
    const string NPC_TAG = "NPC";
    int counter = 0;
    // Use this for initialization
    void Start()
    {
        // Spawn player in a different location
        Vector3 player_placement;
        int playerPlacementIndex = UnityEngine.Random.Range(0, spawnpointsPlayer.Count);
        player_placement = spawnpointsPlayer[playerPlacementIndex].position;
        Instantiate(Player, player_placement, Quaternion.identity);

        ////// Spawn NPCs
        Vector3 placement;
        for (int i = 0; npcs.Count < MAX_NPCs && i < MAX_ATTEMPTS; i++)
        {
            int placementIndex;

            if (counter < 3)
            {
                placementIndex = ((playerPlacementIndex * 5) + UnityEngine.Random.Range(0, 4));
                if (!intList.Contains(placementIndex))
                {
                    intList.Add(placementIndex);
                    placement = spawnpointsCloseRange[placementIndex].position;
                    if (counter < 1)
                    {
                        GameObject npcInstance = Instantiate(npcInput, placement, Quaternion.identity);
                        npcInstance.GetComponent<NavMeshAgent>().stoppingDistance = 0;
                        npcs.Add(npcInstance);
                        //spawnpointsCloseRange.RemoveAt(placementIndex);
                        counter++;
                    }
                    else
                    {
                        GameObject npcInstance = Instantiate(npcInputShoot, placement, Quaternion.identity);
                        npcInstance.GetComponent<NavMeshAgent>().stoppingDistance = 0;
                        npcs.Add(npcInstance);
                        //npcs.Add(Instantiate(npcInputShoot, placement, Quaternion.identity));
                        //spawnpointsCloseRange.RemoveAt(placementIndex);
                        counter++;
                    }
                }

            }
            else if (counter < 6)
            {
                placementIndex = UnityEngine.Random.Range(0, spawnpointsFarRange.Count);
                placement = spawnpointsFarRange[placementIndex].position;
                if (counter < 5)
                {
                    npcs.Add(Instantiate(npcInput, placement, Quaternion.identity));
                    spawnpointsFarRange.RemoveAt(placementIndex);
                    counter++;
                }
                else
                {
                    npcs.Add(Instantiate(npcInputShoot, placement, Quaternion.identity));
                    spawnpointsFarRange.RemoveAt(placementIndex);
                    counter++;
                }
            }
        }
    }
}
