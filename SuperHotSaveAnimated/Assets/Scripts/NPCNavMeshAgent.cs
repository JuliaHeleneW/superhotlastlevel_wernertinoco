﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class NPCNavMeshAgent : MonoBehaviour {

    NavMeshAgent agent;
    //const int EPSILON = 1;
    Animator anim;
    Animator anim2;
    GameObject player;
    public float minRange = 3f;
    float speed = 1f;
    Vector3 lookPosition;
    public bool gotHit = false;
    int counter = 0;
    bool inEyesight;

    GameObject fpsCam;
    CharacterController cCon;
    CapsuleCollider cCol;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsC;
    public GameObject playerCam;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        gotHit = false;
        anim.SetBool("running", true);
        player = GameObject.FindGameObjectWithTag("Player");
        anim2 = GameObject.FindWithTag("PlayerAnim").GetComponent<Animator>();
        agent.SetDestination(player.transform.position);
        agent.stoppingDistance = 0.5f;
        inEyesight = false;

        cCon = GameObject.FindWithTag("PlayerAnim").GetComponent<CharacterController>();
        cCol = GameObject.FindWithTag("PlayerAnim").GetComponent<CapsuleCollider>();
        fpsCam = GameObject.Find("FirstPersonCharacter");
        fpsC= GameObject.FindWithTag("PlayerAnim").GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        playerCam = GameObject.Find("PlayerCamera");

    }
    void Update()
    {
        lookPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
        agent.SetDestination(player.transform.position);
        Vector3 target = new Vector3(fpsC.transform.position.x, transform.position.y, fpsC.transform.position.z);
        if ((Vector3.Distance(transform.position, target) <= minRange))
        {
            transform.LookAt(target);
            anim.SetBool("running", false);
            anim.SetBool("attacking", true);
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Right_Cross_Punch_To_Head_Attacker"))
            {
                transform.LookAt(target + new Vector3(0f, 0f, -2f));
            }
        }
        else
        {
            transform.LookAt(target);
            anim.SetBool("running", true);
            anim.SetBool("attacking", false);
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if ((anim.GetCurrentAnimatorStateInfo(0).IsName("Attacking"))&&(collision.transform.tag == "Player" || collision.transform.tag == "PlayerAvatar" || collision.transform.tag == "PlayerAnim"))
        {
            Debug.Log("animIn");
            if (anim2.GetBool("Dying") == false)
            {
                anim2.SetBool("Dying", true);
                StartCoroutine(DyingAnim());
            }
        }
    }

    private IEnumerator DyingAnim()
    {
        lookPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
        fpsCam.transform.LookAt(lookPosition);
        playerCam.transform.position = fpsCam.transform.position;
        fpsCam.SetActive(false);
        playerCam.GetComponent<DyingCamera>().scriptActive = true;
        //fpsCam.GetComponent<Camera>().fieldOfView *= 10f; 
        cCon.enabled = false;
        cCol.enabled = true;
        fpsC.enabled = false;
        yield return new WaitForSeconds((anim.GetCurrentAnimatorStateInfo(0).length) + 0.5f);
        Debug.Log(anim.GetCurrentAnimatorStateInfo(0).length);
        //Time.timeScale = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
