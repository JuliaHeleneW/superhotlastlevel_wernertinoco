﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCreation : MonoBehaviour {
    //array of game objects for the npc clones
    public List<GameObject> npcs;
    public GameObject npcInput;
    public GameObject npcInputShoot;
    public GameObject Player;
    public List<Transform> spawnpointsCloseRange;
    public List<Transform> spawnpointsFarRange;
    public List<Transform> spawnpointsPlayer;
    const int MAX_NPCs = 6;
    const int MAX_ATTEMPTS = 100;
    const string NPC_TAG = "NPC";
    int counter = 0;
    // Use this for initialization
    void Start()
    {
        // Spawn player in a different location
        Vector3 player_placement;
        int playerPlacementIndex = UnityEngine.Random.Range(0, spawnpointsPlayer.Count);
        player_placement = spawnpointsPlayer[playerPlacementIndex].position;
        Instantiate(Player, player_placement, Quaternion.identity);

        // Spawn NPCs
        Vector3 placement;
        for (int i = 0; npcs.Count < MAX_NPCs && i < MAX_ATTEMPTS; i++)
        {
            bool good_placement = true;
            int placementIndex;
            if (counter < 3)
            {
                placementIndex = (UnityEngine.Random.Range(1, 5) + (playerPlacementIndex * 5)) - 1;
                //placementIndex = UnityEngine.Random.Range(0, spawnpointsCloseRange.Count);
                placement = spawnpointsCloseRange[placementIndex].position;
                spawnpointsCloseRange.RemoveAt(placementIndex);
                if (counter <1)
                {
                    npcs.Add(Instantiate(npcInput, placement, Quaternion.identity));
                }
                else
                {
                    npcs.Add(Instantiate(npcInputShoot, placement, Quaternion.identity));
                }
                counter++;
            }
            else if(counter < 6)
            {
                placementIndex = UnityEngine.Random.Range(0, spawnpointsFarRange.Count);
                Debug.Log(spawnpointsFarRange.Count);
                placement = spawnpointsFarRange[placementIndex].position;
                spawnpointsFarRange.RemoveAt(placementIndex);
                //npcs.Add(Instantiate(npcInput, placement, Quaternion.identity));
                foreach (GameObject npc in npcs)
                {
                    Vector3 origin = placement + new Vector3(0, 1.3f, 0);
                    Vector3 target = npc.transform.position + new Vector3(0, 1.3f, 0);
                    Vector3 direction = target - origin;
                    RaycastHit hit;
                    if (Physics.Raycast(origin, direction, out hit, 200))
                    {
                        if (hit.transform.tag == NPC_TAG || hit.transform == Player.transform)//.tag == "Player")
                        {
                            good_placement = false;
                            Debug.Log("Oops, we are in tha same room!");
                        }
                    }
                }
                spawnpointsFarRange.RemoveAt(placementIndex);
                if (counter < 5)
                {
                    npcs.Add(Instantiate(npcInput, placement, Quaternion.identity));
                }
                else
                {
                    npcs.Add(Instantiate(npcInputShoot, placement, Quaternion.identity));
                }
                counter++;

                //if (good_placement == true)
                //{
                //    spawnpointsFarRange.RemoveAt(placementIndex);
                //    if (counter < 5)
                //    {
                //        npcs.Add(Instantiate(npcInput, placement, Quaternion.identity));
                //    }
                //    else
                //    {
                //        npcs.Add(Instantiate(npcInputShoot, placement, Quaternion.identity));
                //    }
                //    counter++;
                //}
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
