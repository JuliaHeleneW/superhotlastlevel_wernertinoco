﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
    public GameObject player;
    public Transform playerTrans;
    public float maxRange = 30f;
    public float minRange =2f;
    float speed = 1f;
	Vector3 lookPosition;
	public bool gotHit=false;
	//Animator animations;
    // Use this for initialization
    void Start()
    {
		gotHit = false;
		//animations = GetComponent<Animator> ();
		//this.GetComponent<Rigidbody>().centerOfMass=new Vector3(transform.position.x,transform.position.y+10f,transform.position.z);
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
    }

    // Update is called once per frame
    void Update()
    {
		lookPosition = new Vector3 (playerTrans.position.x, 0f, playerTrans.position.z);
		//animations.speed = TimeScale.GetInstance ().gameTimeScale;
        Vector3 origin = transform.position;
        Vector3 target = new Vector3(playerTrans.transform.position.x, transform.position.y, playerTrans.transform.position.z);
        //Vector3 direction = target - origin;
		float step = speed * Time.deltaTime;
		if ((Vector3.Distance(origin, target) <= maxRange) && (Vector3.Distance(origin, target) > minRange)&&gotHit==false)
        {
			//Debug.Log (step * TimeScale.GetInstance ().gameTimeScale);
			transform.position =Vector3.MoveTowards(origin,target,step);
			transform.LookAt (lookPosition);
			//transform.position +=Vector3.forward * Time.deltaTime*speed;
			//transform.LookAt (lookPosition);
        }
    }
}