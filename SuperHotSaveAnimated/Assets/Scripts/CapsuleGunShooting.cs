﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CapsuleGunShooting : MonoBehaviour {
    //public int gunDamage = 1;
    //public float fireRate = 0.25f;
    //public float weaponRange = 70f;
    //public float hitForce = 500000f;
    //public string animationName;
    
    //private AudioSource gunAudio;                                       
    //private LineRenderer laserLine;
    //private float nextFire;

    //actually used:
    public Transform gunEnd;
    public GameObject player;
    public NPCLookRotation rotation;
    public Rigidbody bulletPrefab;
    bool hitWall = false;

    Animator anim;
    private WaitForSeconds shotDuration = new WaitForSeconds(1f);
    bool shotDone;
    Rigidbody bulletInstance;
    NavMeshAgent agent;
    const float maxRange =10f;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        rotation = GetComponent<NPCLookRotation>();
        player = GameObject.Find("MHumanPlayer");
        shotDone = true;
        bulletInstance = null;
        anim.SetBool("reachedBool", false);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rayOrigin = transform.position;
        Vector3 targetRay = player.transform.position;
        RaycastHit hit;
        if (Physics.Raycast(rayOrigin, targetRay, out hit, 200))
        {
            if(hit.transform.tag=="Wall")
            {
                hitWall = true;
            }
            else
            {
                hitWall = false;
            }

        }


        Vector3 target = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
        if (Vector3.Distance(transform.position, target) >= maxRange||hitWall==true)
        {
            anim.SetBool("reachedBool", false);
            agent.SetDestination(target);
        }
        else
        {
            anim.SetBool("reachedBool", true);
        }



        AnimatorStateInfo currentState = anim.GetCurrentAnimatorStateInfo(0);
        float frameTime = currentState.normalizedTime * currentState.length;
        // Debug.Log(frameTime);
        // when weapon shoot animation is activated
        if (currentState.IsName("WeaponLower") && frameTime > 0.0f && frameTime < 1f)
        {
            Vector3 lookPosition = player.transform.position;
            anim.SetLookAtPosition(lookPosition);
            //Debug.Log("check1");
            //Rigidbody bulletInstance=null;
            if (shotDone == true)//&&rotation.gotLookedAt==true)
            {
                Time.timeScale = Mathf.Lerp(Time.timeScale, 0.001f, 0.1f);
                Time.fixedDeltaTime = 0.02f;
                Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, 0.1f);
                Time.fixedDeltaTime = Time.timeScale * 0.02f;
                //Debug.Log("check2");
                bulletInstance = Instantiate(bulletPrefab, gunEnd.position, Quaternion.Euler(90, 75, 0)) as Rigidbody;
                bulletInstance.velocity = new Vector3(0.1f, 0.1f, 0.1f);
                //Vector3 direction = (gunEnd.forward * 1000f) * Time.deltaTime;
                bulletInstance.AddForce(gunEnd.forward * 100f);


                //Debug.Log(bulletInstance.position);
                //bulletInstance.AddForce(gunEnd.forward * 1000f);

                //bulletInstance.GetComponent<BulletScript>().gunEnd = gunEnd;
                StartCoroutine(ShotEffect());
                //Debug.Log("check4");
                //Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, 0.1f);
                //Time.fixedDeltaTime = Time.timeScale * 0.02f;
            }


        }
    }

    private IEnumerator ShotEffect()
    {
        shotDone = false;
        yield return shotDuration;
        shotDone=true;
    }
}
